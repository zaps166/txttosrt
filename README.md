23 September 2011

---

Program TxtToSrt napisany w języku C służy do konwersji zwykłych napisów do SRT. Format napisów obsługiwanych przez program:
- 00:09:10: Napiski
- 01:12:23:Jakiś tekst|Następnia linia jakiegoś tekstu

Jako parametr podaje się plik wejściowy, wyjściowy ( opcjonalnie ) oraz maksymalną długość wyświetlania napisów. Parametr `--duration 6` oznacza, że długość wyświetlania napisów wynosi 6 sekund, jeżeli następne napisy nie pojawią się wcześniej. Kodowanie napisów nie ma znaczenia, a plik wyjściowy będzie posiadał zakończenie linii `\n` (LF).

Zalecane polecenie do kompilacji (GCC): `gcc txtToSrt.c -O2 -s -o txtToSrt`
