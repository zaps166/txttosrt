#ifndef __cplusplus
	typedef unsigned char bool;
	#define false 0
	#define true 1
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct Subtitle
{
	char *txt;
	int Start;
	bool error;
};

static char *get_line( FILE *f )
{
	char *tmp = NULL;
	size_t count = 0;
	for ( ;; )
	{
		int c = fgetc( f );
		bool end = false;
		if ( c < 0 || c == '\r' || c == '\n' )
			end = true;
		if ( end && !count )
			break;
		tmp = ( char * )realloc( tmp, count + 1 );
		if ( end )
		{
			tmp[ count ] = '\0';
			break;
		}
		tmp[ count ] = c;
		count++;
	}
	return tmp;
}

static void _quickSort( struct Subtitle *lista, int lewy, int prawy )
{
	int v = lista[ ( lewy + prawy ) / 2 ].Start;
	struct Subtitle x;
	int i = lewy;
	int j = prawy;
	do
	{
		while ( lista[ i ].Start < v )
			i++;
		while ( lista[ j ].Start > v )
			j--;
		if ( i <= j )
		{
			x = lista[ i ];
			lista[ i ] = lista[ j ];
			lista[ j ] = x;
			i++;
			j--;
		}
	} while ( i <= j );
	if ( j > lewy )
		_quickSort( lista, lewy, j );
	if ( i < prawy )
		_quickSort( lista, i, prawy );
}
static inline void quickSort( struct Subtitle *lista, int count )
{
	_quickSort( lista, 0, count - 1 );
}

static void free_subs( struct Subtitle *subs, size_t count )
{
	size_t i = 0;
	for ( ; i < count ; i++ )
	{
		if ( !subs[ i ].error )
			free( subs[ i ].txt );
	}
	free( subs );
}

int main( int argc, char *argv[] )
{
	printf( "Tomo TXT to SRT subtitles converter ( http://zaps166.sf.net/ )\n" );

	unsigned int i, current_line = 0;

	unsigned int duration = 6;
	const char *file_in = NULL;
	char *file_out = NULL;

	for ( i = 1 ; i < argc ; i++ )
	{
		if ( !strcmp( argv[ i ], "--help" ) )
		{
			printf( " Example 1: ./txtToSrt asd.txt\n Example 2: ./txtToSrt asd.txt zxc.srt --duration 5" );
			return 0;
		}
		else if ( !strcmp( argv[ i ], "--duration" ) )
		{
			i++;
			if ( i < argc )
				duration = atoi( argv[ i ] );
		}
		else if ( !file_in )
			file_in = argv[ i ];
		else if ( !file_out )
			file_out = argv[ i ];
	}

	if ( file_in && !file_out )
	{
		size_t len = strlen( file_in );
		if ( !strncmp( file_in + len - 4, ".txt", 4 ) )
			len -= 4;
		file_out = ( char * )malloc( len + 5 );
		memset( file_out, 0, len + 5 );
		memcpy( file_out, file_in, len );
		strcat( file_out, ".srt" );
	}
	printf( "\nMaximum duration: %i\nInput file: %s\nOutput file: %s\n\n", duration, file_in, file_out );

	FILE *f;

	f = fopen( file_in, "rb" );
	if ( !f )
	{
		perror( "Cannot open input file" );
		return -1;
	}

	struct Subtitle *subs = NULL;
	int count = 0;
	while ( !feof( f ) )
	{
		char *line = get_line( f );
		char *txt = NULL;
		if ( !line )
			continue;
		size_t len = strlen( line );
		if ( len < 10 )
		{
			free( line );
			continue;
		}
		int h, m, s;
		sscanf( line, "%d:%d:%d", &h, &m, &s );
		if ( h < 0 || m < 0 || s < 0 || h > 99 || m > 59 || s > 59 )
		{
			free( line );
			continue;
		}
		for ( i = 8 ; i < len ; i++ )
		{
			if ( line[ i ] != ' ' && line[ i ] != ':' )
			{
				txt = strdup( line + i );
				for ( i = 0 ; i < strlen( txt ) ; i++ )
				{
					if ( txt[ i ] == '|' )
						txt[ i ] = '\n';
				}
				break;
			}
		}
		free( line );
		if ( !txt )
			continue;
		subs = ( struct Subtitle * )realloc( subs, sizeof( struct Subtitle ) * ( count + 1 ) );
		subs[ count ].txt = txt;
		subs[ count ].Start = h * 3600 + m * 60 + s;
		subs[ count ].error = false;
		count++;
	}
	fclose( f );

	if ( !subs )
	{
		fprintf( stderr, "No subtitles!" );
		return -1;
	}

	quickSort( subs, count );
	for ( i = 0 ; i < count ; i++ )
	{
		int j = i + 1;
		if ( subs[ count ].error )
			continue;
		for ( ; j < count ; j++ )
		{
			if ( subs[ i ].Start == subs[ j ].Start )
			{
				subs[ i ].txt = ( char * )realloc( subs[ i ].txt, strlen( subs[ i ].txt ) + 1 + strlen( subs[ j ].txt ) + 1 );
				strcat( subs[ i ].txt, "\n" );
				strcat( subs[ i ].txt, subs[ j ].txt );
				free( subs[ j ].txt );
				subs[ j ].error = true;
			}
		}
	}

	f = fopen( file_out, "wb" );
	if ( !f )
	{
		free_subs( subs, count );
		perror( "Cannot open output file" );
		return -1;
	}

	for ( i = 0 ; i < count ; i++ )
	{
		int j = i + 1;
		if ( subs[ i ].error )
			continue;
		struct Subtitle *next_subtitle = NULL;
		for ( ; j < count ; j++ )
		{
			if ( !subs[ j ].error )
			{
				next_subtitle = &subs[ j ];
				break;
			}
		}
		int Start = subs[ i ].Start;
		int End = Start + duration;
		if ( next_subtitle && End > next_subtitle->Start )
			End = next_subtitle->Start;
		int hS = Start / 3600;
		int mS = Start % 3600 / 60;
		int sS = Start % 60;
		int hE = End / 3600;
		int mE = End % 3600 / 60;
		int sE = End % 60;
		fprintf( f, "%u\n%.2d:%.2d:%.2d,000 --> %.2d:%.2d:%.2d,000\n%s\n", ++current_line, hS, mS, sS, hE, mE, sE, subs[ i ].txt );
		if ( next_subtitle )
			fputc( '\n', f );
	}

	free_subs( subs, count );
	fclose( f );
	return 0;
}
